# The www.debconf.org website

## Deploying:

Only members of `debconfstatic` can deploy, so ping one of them to
deploy.

On static-master (dillon.debian.org) as `debconfstatic`:

1. `cd /srv/debconf-webpages/git/www`
1. `git pull --ff-only`
1. `make`
1. `static-update-component www.debconf.org`
